import graphelib


def list_prec(depart, arrivee, prec):
    way = []
    graphe = graphelib.Graphe()
    numDep = arrivee
    while numDep != depart:
        way.append(graphe.dic_noeud[numDep])  #ajout dans le chemin le numDep visiter
        numDep = prec[numDep].numero  #prend la valeur du noeud prec
    way.append(graphe.dic_noeud[depart])
    return way
