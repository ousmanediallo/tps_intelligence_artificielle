import graphelib
import numpy as np


def Astar(graphe, depart, arrivee):
    E = [graphe.dic_noeud[depart]]

    d, prec, f = {}, {}, {}
    Cpt_astar = 0
    for a in graphe.dic_noeud:
        d[a] = np.infty
        f[a] = np.infty
        prec[a] = None
    d[depart] = 0
    f[depart] = graphelib.distance_km(graphe.dic_noeud[depart], graphe.dic_noeud[arrivee])

    while len(E) != 0:
        u = E[0]
        Cpt_astar += 1
        for i in range(len(E)):
            new_val = f[E[i].numero] < f[u.numero]
            if new_val:
                u = E[i]
        if u.numero == arrivee:
            return d[u.numero], Cpt_astar, prec
        E.remove(u)

        for v in u.aretes:
            alt = d[u.numero] + v[1]    #v[1] val and v[0] le noeud
            new_val2 = alt < d[v[0].numero]
            if new_val2:
                d[v[0].numero] = alt
                f[v[0].numero] = d[v[0].numero] + graphelib.distance_km(v[0], graphe.dic_noeud[arrivee])
                prec[v[0].numero] = u
                if v[0] not in E:
                    E.append(v[0])    #E.add(v) dans le cas d'un dictionnaire











