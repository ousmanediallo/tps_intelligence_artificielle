import matplotlib.pyplot as plt

import graphelib as graphe
import dijkstra
import astar
import listprec_chemins

Graphe = graphe.Graphe()
Carte = graphe.Carte(Graphe)

# Pour Dijkstra
d, Cpt_dijkstra, Dijkstra = dijkstra.Dijkstra(Graphe, 29, 73)
Way2 = listprec_chemins.list_prec(29, 73, Dijkstra)
Carte.afficher_chemin(Way2, "purple")
print("Compteur de Dijkstra:", Cpt_dijkstra)
plt.show()


# Pour Astar
a, Cpt_astar, Astar = astar.Astar(Graphe, 66, 95)
Way = listprec_chemins.list_prec(66, 95, Astar)
Carte.afficher_chemin(Way, "green")
print("Compteur de Astar:", Cpt_astar)
plt.show()


