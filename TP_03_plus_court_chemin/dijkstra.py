import graphelib
import numpy as np


def Dijkstra(graphe, depart, arrivee):
	E = []
	prec, d = {}, {}
	Cpt_dijkstra = 0
	for a in graphe.dic_noeud.values():  #a est une cle du departement
		E.append(a)   #E[a] = a

	for i in range(len(E)):
		d[E[i].numero] = np.infty
		prec[E[i].numero] = None
	d[depart] = 0

	while len(E) != 0:
		u = E[0]
		Cpt_dijkstra += 1
		for i in range(len(E)):
			new_val = d[E[i].numero] < d[u.numero]
			if new_val:
				u = E[i]
		if u.numero == arrivee:
			return d[u.numero], Cpt_dijkstra, prec
		E.remove(u)

		for v in u.aretes:
			alt = d[u.numero] + v[1]  #v[0] le noeud
			new_val2 = alt < d[v[0].numero]
			if new_val2:
				d[v[0].numero] = alt
				prec[v[0].numero] = u