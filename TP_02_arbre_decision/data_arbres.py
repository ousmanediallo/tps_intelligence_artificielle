import math

import numpy as np


class DataPoint:
    def __init__(self, x, y, cles):
        self.x = {}
        for i in range(len(cles)):
            self.x[cles[i]] = float(x[i])
        self.y = int(y)
        self.dim = len(self.x)
        
    def __repr__(self):
        return 'x: '+str(self.x)+', y: '+str(self.y)


def load_data(filelocation):
    with open(filelocation, 'r') as f:
        data = []
        attributs = f.readline()[:-1].split(',')[:-1]
        for line in f:
            z = line.split(',')
            if z[-1] == '\n':
                z = z[:-1]
            x = z[:-1]
            y = int(z[-1])
            data.append(DataPoint(x, y, attributs))
    return data


class Noeud:
    def __init__(self, profondeur_max=np.infty, hauteur=0):
        self.question = None
        self.enfants = {}
        self.profondeur_max = profondeur_max
        self.proba = None
        self.hauteur = hauteur  # ajout de la hauteur

        self.gain_E = None

    def prediction(self, x):
        if self.question is None:
            return self.proba
        if question_inf(x, self.question[0], self.question[1]):
            return self.enfants[0].prediction(x)
        return self.enfants[1].prediction(x)

    def grow(self, data):  # data: DataPoint
        entropiee = entropie(data)
        a, s = best_split(data)
        self.proba = proba_empirique(data)
        d1, d2 = split(data, a, s)  # split[0], split[1]
        if entropiee > 0 and not self.profondeur_max > 0 and len(d1) > 0 and len(d2) > 0:
            self.question = (a, s)  # enregistre la question

            self.enfants[True] = Noeud(self.profondeur_max-1, self.hauteur+1)
            self.enfants[True].grow(d1)
            self.enfants[False] = Noeud(self.profondeur_max-1, self.hauteur+1)
            self.enfants[False].grow(d2)
        else:
            self.proba = proba_empirique(data)
            self.gain_E = gain_entropie(data, a, s)

    def elagage(self, alpha):
        if self.gain_E < alpha:
            if self.enfants:
                noeud1 = self.enfants[True].propa
                noeud2 = self.enfants[False].propa
                self.propa = [(noeud1[0] + noeud2[0])/2,
                              (noeud1[1] + noeud2[1])/2]
                del self.enfants
        else:
            for i in self.enfants.values():
                i.elagage(alpha)

    def __del__(self):
        del self.enfants


def proba_empirique(d):  # d: datapoint
    liste_dict = {}
    s = 0
    for i in range(len(d)):
        if d[i].y in liste_dict:
            liste_dict[d[i].y] += 1
        else:
            liste_dict[d[i].y] = 1
    for j in liste_dict:
        s += liste_dict[j]
    for n in liste_dict:
        liste_dict[n] /= s
    return liste_dict


def question_inf(x, a, s):
    return x[a] < s


def split(d, a, s):
    d1, d2 = [], []
    for i in range(len(d)):
        verif = question_inf(d[i].x, a, s)
        if verif:
            d1.append(d[i])
        else:
            d2.append(d[i])
    return d1, d2


def list_separ_attributs(d, a):
    l1, l2 = [], []
    for i in range(len(d)):
        l1.append(d[i].x[a])
    l1 = set(l1)
    l1 = list(l1)
    l1.sort()
    for i in range(len(l1) - 1):
        q = (l1[i] + l1[i+1]) / 2
        l2.append([a, q])
    return l2


def list_questions(d):  # d: datapoint
    liste = []
    dict_attribut = d[0].x  # or d[0].x.keys()
    for i in dict_attribut:
        liste += list_separ_attributs(d, i)
    return liste


def entropie(d):  # d: datapoint
    proportion = 0
    proportion2 = proba_empirique(d)
    for i in proportion2:
        if proportion2[i] != 0:
            proportion += - proportion2[i] * math.log2(proportion2[i])
    return proportion


def gain_entropie(dataPoint, a, s):
    splite = split(dataPoint, a, s)
    d1, d2 = splite[0], splite[1]
    r1, r2 = len(d1) / len(dataPoint), len(d2) / len(dataPoint)
    gain = entropie(dataPoint) - r1 * entropie(d1) - r2 * entropie(d2)
    return gain


def best_split(d):  # d: datapoint
    maxi = 0
    best = list_questions(d)
    val = ()
    for i in best:
        gainEntropie = gain_entropie(d, i[0], i[1])
        if maxi < gainEntropie:
            maxi = gainEntropie
            val = (i[0], i[1])
    return val


def precision(N, d):  # N: noeud, d: datapoint
    pourcentage_predit = 0
    for i in range(len(d)):
        if N.prediction(d[i].x) == d[i].y:
            pourcentage_predit += 1
    return pourcentage_predit / len(d)























