import numpy.random
import interfacelib
import numpy as np
import jeulib

PROFONDEUR = 5

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interfacelib.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts
		self.compteur = 0
		if "compteur" in opts:
			self.compteur = opts["compteur"]
		else:
			self.compteur = 1

	def ajoutCompteur(self):
		self.compteur += 1

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass


class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie, couleur, opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0

class Random(IA):
	
	def demande_coup(self):
		liste_valide = self.jeu.plateau.liste_coups_valides(self.couleurval)
		return liste_valide[np.random.randint(0, len(liste_valide))]


class Minmax(IA):

	def demande_coup(self):
		coups = self.minmax(self.couleurval, self.jeu.plateau, 2)
		return coups[1]

	def minmax(self, couleur, plateau, depth):
		if depth <= 0 or (not (plateau.existe_coup_valide(couleur)) and not (plateau.existe_coup_valide(-couleur))):
			return (plateau.evaluation(self.couleurval), [])
		if self.couleurval == couleur:
			coups_valide = plateau.liste_coups_valides(couleur)
			M = (-np.Infinity)
			for E in coups_valide:
				nouveau_plateau = plateau.copie()
				nouveau_plateau.jouer(E, couleur)
				val = self.minmax(-couleur, nouveau_plateau, depth-1)[0]
				if M < val:
					M = val
					argmax = E
			return (M, argmax)
		else:
			coups_valide = plateau.liste_coups_valides(-couleur)
			m = np.Infinity
			for E in coups_valide:
				nouveau_plateau = plateau.copie()
				nouveau_plateau.jouer(E, couleur)
				val = self.minmax(-couleur, nouveau_plateau, depth-1)[0]
				if m > val:
					m = val
					argmin = E
			return (m, argmin)


class AlphaBeta(IA):

	def demande_coup(self):
		coups = self.alphabeta(self.couleurval, self.jeu.plateau, (-np.Infinity), np.Infinity, 2)
		return coups[1]

	def alphabeta(self, couleur, plateau, alpha, beta, depth):
		if depth <= 0:
			return (plateau.evaluation(self.couleurval), [])
		if self.couleurval == couleur:
			coupValide = plateau.liste_coups_valides(couleur)
			M = (-np.Infinity)
			for E in coupValide:
				nouveau_plateau = plateau.copie()
				nouveau_plateau.jouer(E, couleur)
				val = self.alphabeta(-couleur, nouveau_plateau, alpha, beta, depth-1)[0]
				if M < val:
					M = val
					argmax = E
					alpha = max(alpha, M)
				if alpha >= beta:
					break
			return (M, argmax)
		else:
			coupValide = plateau.liste_coups_valides(-couleur)
			m = np.Infinity
			for E in coupValide:
				nouveau_plateau = plateau.copie()
				nouveau_plateau.jouer(E, couleur)
				val = self.alphabeta(-couleur, nouveau_plateau, alpha, beta, depth-1)[0]
				if m > val:
					m = val
					argmin = E
					beta = min(beta, m)
				if alpha >= beta:
					break
			return (m, argmin)


class AlphaBetaSecond(IA):

	def demande_coup(self):
		coupValide = self.alphabeta(self.couleurval, self.jeu.plateau.copie(), (-np.Infinity), np.Infinity, self.compteur)
		if coupValide[1] == np.Infinity or coupValide[1] == -np.Infinity:
			return None
		return coupValide[0]

	def alphabeta(self, couleurs, plateau, alpha, beta, depth):
		if not plateau.existe_coup_valide(couleurs) and not plateau.existe_coup_valide(couleurs):
			return ([], evalPosition(plateau, couleurs, []))
		elif couleurs == self.couleurval:
			M = ([], -np.Infinity)
			coupValides = sorted(plateau.liste_coups_valides(couleurs),
								 key = lambda d: evalPosition(plateau, couleurs, d), reverse = True)
		else:
			M = ([], np.Infinity)
			coupValides = sorted(plateau.liste_coups_valides(couleurs),
								 key=lambda d: evalPosition(plateau, couleurs, d), reverse = False)
		for E in coupValides:
			if depth <= 1:
				val = (E, (evalPosition(plateau, couleurs, E)))
			else:
				nouveau_plateau = plateau.copie()
				nouveau_plateau.jouer(E, couleurs)
				val = (E, self.alphabeta(-1*couleurs, nouveau_plateau, depth - 1)[1])
				self.ajoutCompteur()
			if couleurs == self.couleurval:
				if M[1] < val[1]:
					M = val
					argmax = val[0]
					alpha = max(alpha, M[1])
				if alpha >= beta:
					break
				return (argmax, M[1])
			else:
				if M[1] > val[1]:
					M = val
					argmin = val[0]
					beta = min(beta, M[1])
				if alpha >= beta:
					break
				return (argmin, M[1])


#Methode evaluation position de AlphaBetaSecond
def evalPosition(plateau, couleurs, pos):
	noirJoueur, blancJoueur = 0, 0
	nouveau_plateau = plateau.copie()

	for i in range(nouveau_plateau.taille):
		for j in range(nouveau_plateau.taille):
			if nouveau_plateau.tableau_cases[i][j] == couleurs:
				noirJoueur += 1
			if nouveau_plateau.tableau_cases[i][j] == -couleurs:
				blancJoueur += 1
	score1 = noirJoueur - blancJoueur

	noirJoueurN, blancJoueurB = 0, 0
	nouveau_plateau.jouer(pos, couleurs)

	for i in range(nouveau_plateau.taille):
		for j in range(nouveau_plateau.taille):
			if nouveau_plateau.tableau_cases[i][j] == couleurs:
				noirJoueur += 1
			if nouveau_plateau.tableau_cases[i][j] == -couleurs:
				blancJoueur += 1
	score2 = noirJoueurN - blancJoueurB

	if (pos != []):
		if (pos == [0, 0]
				or pos == [plateau.taille-1, 0]
				or pos == [0, plateau.taille-1]
				or pos == [plateau.taille-1, plateau.taille-1]
		):
			score2 += 2
		if (pos[0] == 0
				or pos[0] == plateau.taille - 1
				or pos[1] == 0
				or pos[1] == plateau.taille-1
		):
			score2 += 1
		Score = (score2 - score1) - len(plateau.liste_coups_valides(-couleurs))
		return Score



