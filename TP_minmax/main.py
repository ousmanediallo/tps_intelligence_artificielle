import jeulib
import joueurlib
import time
import matplotlib.pyplot as plt

jeu = jeulib.Jeu()
jeu.demarrer()
temps, coup, compteur = [], [], []

jnoir = joueurlib.AlphaBeta(jeulib, "noir", {
    'choix_joueurs': False, 'interface': False, "compteur": 1})
jblanc = joueurlib.Random(jeulib, "blanc", {
    'choix_joueurs': False, 'interface': False, "compteur": 2})


def simuler(n, IA, IA2):
    blanc, noir, tempsTime = [], [], []

    for i in range(n):
        nouveau_jeu = jeulib.Jeu({
            # d'abord le choix du joueur et l'interface dans jeulib a "false"
            "choix_joueurs": False,
            "interface": False})
        IA.jeu = nouveau_jeu
        IA2.jeu = nouveau_jeu

        nouveau_jeu.noir = IA
        nouveau_jeu.blanc = IA2

        nouveau_jeu.joueur_courant = IA  #joueur Noir par defaut
        nouveau_jeu.demarrer()

        noir.append(nouveau_jeu.noir.compteur)
        blanc.append(nouveau_jeu.blanc.compteur)
        tempsTime.append(time.time() - nouveau_jeu.time)
        print(str(i+1)+"/"+str(n))

    MoyenneCampNoir = sum(noir)/len(noir)
    MoyenneCampBlanc = sum(blanc)/len(blanc)
    MoyennetempsTime = sum(tempsTime)/len(tempsTime)
    MTT = (MoyenneCampNoir + MoyenneCampBlanc)/2

    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("Moyenne pour le camp noir : " + str(MoyenneCampNoir))
    print("Moyenne pour le camp blanc : " + str(MoyenneCampBlanc))
    print("Moyenne des temps : " + str(MoyennetempsTime))
    print("Moyenne pour tout les camps : " + str(MTT))
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
    return [MoyennetempsTime, MTT]


for q in range(1, 5):
    jnoir.compteur = q
    jblanc.compteur = q
    simulation = simuler(5, jnoir, jblanc)
    temps.append(simulation[0])
    coup.append(simulation[1])
    compteur.append(q)

# Premiere courbe
plt.subplot(3, 1, 1)
plt.plot(compteur, temps)
plt.xlabel("tourDuJoueur")
plt.ylabel("depth")

# Deuxieme courbe
plt.subplot(3, 1, 3)
plt.plot(compteur, coup, "r-")
plt.xlabel("temps")
plt.ylabel("depth")

plt.show()


