TPs_INTELLIGENCE_ARTIFICIELLE

 - Ousmane Bobo DIALLO

### TP_01: [tp_minamx](TP_minmax)
 - Implémentations de l'algorithme minmax et ALpha-Beta OK.
 - Le compteur à pour effet de limiter le nombre de fois que le Joueur utilise la methode "jouer()" de la classe plateau,
 - Nouvelle version alpha-beta (AlphaBetaSecond) OK,
 - Traçage des courbe et affichage OK,
 - Amélioration de la methode evaluation OK
 - Bref : Fin du TP_01 atteint.

### TP_02: [tp_arbre_decision](TP_02_arbre_decision)
 - Toute les fonctions ont été implementés,
 - Sauf le traçage des courbes de la question (13 et 15).
 - Bref : Fin du TP_02 atteint.

### TP_03: [tp_plus_court_chemin](TP_03_plus_court_chemin)
 - Implementations de Dijkstra ok,
 - Implementations de Astar ok,
 - Implementations des Precdents (voir listprec_chemins) OK,
 - Ajout des compteurs OK,
 - Affichages Ok. 
 - Bref : Fin du TP_03 atteint.
